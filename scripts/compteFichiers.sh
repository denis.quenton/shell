cd ..
if [ $# -eq 0 ]
then
echo " Vous devez passer des noms de répertoires en paramètres "
exit 1
fi
while [ $# -gt 0 ]
do
 if test ! -f $1
 then
  echo "le fichier $1 n'existe pas "
 fi

 if test ! -d $1
 then
  echo " $1 n'est pas un répertoire "
  exit 1
 else
	 nbdir=$(ls -l $1 | grep -e ^d[rwx-] | wc -l) 
	 nbfich=$(ls -l $1 | grep -e ^-[rwx-] | wc -l)
	 echo " le répertoire $1 contient $nbdir sous-répertoire(s) et $nbfich fichier(s)"
 fi
shift
done
